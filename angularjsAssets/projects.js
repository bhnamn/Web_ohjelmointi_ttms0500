(function() {
	var app = angular.module('page-directives', []);
	app.directive("projectDescription", function() {
		return {
			restrict: 'E',
			scope: {
				item: "="
			},
			templateUrl: "project-description.html"
		};
	});
	app.directive("projectReviews", function() {
		return {
			restrict: 'E',
			scope: {
				reviewedProject: "="
			},
			templateUrl: "project-reviews.html"
		};
	});
	app.directive("projectSpecs", function() {
		return {
			restrict: "A",
			scope: {
				item: "="
			},
			templateUrl: "project-specs.html"
		};
	});
	app.directive("tabGroup", function() {
		return {
			restrict: "E",
			transclude: true,
			templateUrl: "project-tabs.html",
			controller: function($scope) {
				$scope.tabs = [];
				this.addTab = function(tab) {
					if($scope.tabs.length === 0) {
						tab.selected = true;
					}
					$scope.tabs.push(tab);
				};
				this.select = function(selectedTab) {
					angular.forEach($scope.tabs, function(tab) {
						tab.selected = angular.equals(tab, selectedTab);
					});
				}
			},
			controllerAs: "tabGroup"
		};
	});
	app.directive("tab", function() {
		return {
			restrict: "E",
			scope: {
				title: "@"
			},
			transclude: true,
			template: "<div ng-show='selected' ng-transclude=''></div>",
			require: "^tabGroup",
			link: function(scope, element, attrs, ctrl) {
				ctrl.addTab(scope);
			}
		};
	});
	app.directive("projectGallery", function() {
		return {
			restrict: "E",
			templateUrl: "project-gallery.html",
			controller: function() {
				this.current = 0;
				this.setCurrent = function(imageNumber) {
					this.current = imageNumber || 0;
				};
			},
			controllerAs: "gallery"
		};
	});
})();