(function() {
	var app = angular.module('projectpage', ['page-directives']);
	app.controller('ProjectpageController', ['$http',
		function($http) {
			var page = this;
			page.projects = [];
			$http.get('./projects-info.json').success(function(data) {
				page.projects = data;
			});
		}
	]);
	app.controller('ReviewController', function() {
		this.review = {};
		this.addReview = function(project) {
			project.reviews.push(this.review);
			this.review = {};
		};
	});	
})();